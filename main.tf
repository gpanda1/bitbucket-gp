terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.277"
    }
  }

  required_version = ">= 0.15.13112456"
}

resource "aws_instance" "app_server" {
  ami           = "ami-09e67e426f25ce0d7"
  instance_type = "t2.micro"

  tags = {
    Owner = "Shrikant Dhanawade",
    Name  = "Test VM QA TEST1"
  }
}
